//Дана строка, состоящая из символов “(“ и “)”
//        Необходимо написать метод, принимающий эту строку и выводящий результат,
//        является ли она правильной скобочной последовательностью или нет.
//        Строка является правильной скобочной последовательностью, если:
//        ● Пустая строка является правильной скобочной последовательностью.
//        ● Пусть S — правильная скобочная последовательность, тогда (S) есть
//        правильная скобочная последовательность.
//        ● Пусть S1, S2 — правильные скобочные последовательности, тогда S1S2
//        есть правильная скобочная последовательность.


import java.util.Scanner;

public class Dop1_2 {
    public static class Check {
        public boolean check(String str) {
            if (str == null || str.length() % 2 == 1) return false;
            if (!(str.contains("()") || str.contains("{}") || str.contains("[]"))) return false;
            String rezult = str.replaceAll("[(][)]|[{][}]|[\\[][\\]]", "");
            if (rezult.length() == 0) return true;
            return check(rezult);

        }

        public class Main {
            public static void main(String[] args) {
                Scanner scanner = new Scanner(System.in);
                String str = scanner.nextLine();
                Check check = new Check();
                System.out.println(check.check(str));
            }
        }

    }

}
