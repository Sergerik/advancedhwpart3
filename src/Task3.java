import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Task3 {
    public static void main(String[] args) {
        Class<APrinter> cls = APrinter.class;
        try {
            Method method = cls.getDeclaredMethod("print", int.class);
            method.invoke(new APrinter(), -100);
        } catch (IllegalAccessException e) {
            System.out.println("Ошибка доступа к методу");
            throw new RuntimeException(e);
        } catch (IllegalArgumentException e) {
            System.out.println("Неправильный аргумент в методе");
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            System.out.println("Ошибка внутри метода");
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e) {
            System.out.println("Не найден метод заданным именем");
            throw new RuntimeException(e);
        }
    }

    public static class APrinter {
        public void print(int a) {
            System.out.println(a);
        }
    }


}
