
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
//Написать метод, который с помощью рефлексии получит все интерфейсы
//        класса, включая интерфейсы от классов-родителей и интерфейсов-родителей.

public class Task4 {

    public static void main(String[] args) {
        Class<Chaild> cls = Chaild.class;
        List<Class<?>> rezult = allInterfaceOfClass(cls);
        for (Class<?> r : rezult
        ) {
            System.out.println(r.getSimpleName());
        }
    }

    public static List<Class<?>> allInterfaceOfClass(Class<?> cls) {
        List<Class<?>> rezult = new ArrayList<>();
        while (cls != Object.class) {
            rezult.addAll(allInterfaceOfInterface(cls));
            cls = cls.getSuperclass();
        }
        return rezult;
    }

    public static List<Class<?>> allInterfaceOfInterface(Class<?> cls) {
        List<Class<?>> rezult = new ArrayList<>();
        List<Class<?>> intefaceses = Arrays.asList(cls.getInterfaces());
        if (intefaceses.size() == 0) return rezult;
        rezult.addAll(intefaceses);
        for (Class<?> aClass : intefaceses
        ) {
            rezult.addAll(allInterfaceOfInterface(aClass));
        }
        return rezult;
    }

    public class Chaild extends Parent implements CInterface, GIInterface {

    }

    public interface CInterface extends BInterface {
    }

    public interface AInterface {
    }

    public interface BInterface extends AInterface {
    }

    public interface DInterface {
    }

    public interface EInterface extends DInterface {
    }

    public interface GIInterface {
    }

    public class Parent extends PreParent {
    }

    public class PreParent implements EInterface {
    }


}
