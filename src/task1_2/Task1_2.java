package task1_2;
//Создать аннотацию @IsLike, применимую к классу во время выполнения
//        программы. Аннотация может хранить boolean значение.


import jdk.incubator.vector.VectorOperators;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class Task1_2 {
    public static void main(String[] args) {
        methodContainsAnnotationIsLike(VectorOperators.Test.class);
    }

    private static void methodContainsAnnotationIsLike(Class<?> cls) {
        if (cls.isAnnotationPresent(intf.IsLike.class)) {
            intf.IsLike isLike = cls.getAnnotation(intf.IsLike.class);
            System.out.println(isLike.b);
        } else System.out.println("Класс не подписан аннотацией @IsLike");
    }

    @Retention(RetentionPolicy.RUNTIME)
    public @interface IsLike {

        boolean b = true;

    }

    }
